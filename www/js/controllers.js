angular.module('starter.controllers', [])


.controller('AppCtrl', function($scope, $ionicModal, $timeout , $rootScope,$ionicHistory,$ionicPopup,$localStorage,$ionicSideMenuDelegate) {

	$scope.logoBackHome = function()
	{
		window.location ="#/app/main";
	}

	$scope.goBackButton = function()
	{
		 $ionicHistory.goBack();
		//window.history.back();
	}

})

.controller('MainCtrl', function($scope,$rootScope,$localStorage,$ionicSideMenuDelegate,SendGetRequestServer,$ionicPopup,$ionicModal,$translate,$ionicLoading,$ImageCacheFactory,$timeout)
{
	$scope.phpHost = $rootScope.laravelHost;

  $scope.$on('$ionicView.enter', function(e) {
	  $rootScope.DefaultActiveTab = 1;
  });


	$rootScope.CityDetails = [];
	$scope.isLoaded = 0;
	$scope.imagesLoadCount = 0;
	$scope.showSplashImage = true;


	$ionicLoading.show({
	  template: '<ion-spinner icon="lines" class="spinner-assertive"></ion-spinner>',
	  noBackdrop : false,
	  //duration : 10000
	});

  /*
  $scope.$on('imageloaded', function(events, args){
	$ionicLoading.hide();
  })
  */
	$scope.selectCity = function(index)
	{
		$rootScope.selectedCity = index;
		window.location ="#/app/citydetails/"+index;
	}


	$scope.getCities = function()
	{
		$scope.sendparams = {};
		SendGetRequestServer.run($scope.sendparams,$rootScope.laravelHost+'/api/GetCitiesClient',1).then(function(data) {
			$scope.citiesArray = data.cities;
			$rootScope.MainCitiesArray = data;
			console.log("citiesarray: ", $scope.citiesArray);

			setTimeout(function() {
				navigator.splashscreen.hide();
			}, 2000);
		});
	}



	$scope.getCities();

})

.controller('CityDetailsCtrl', function($scope,$rootScope,$stateParams,$localStorage,$ionicSideMenuDelegate,SendGetRequestServer,SendPostRequestServer,$ionicPopup,$ionicModal,$translate,MediaManager,$ionicSlideBoxDelegate,$cordovaGeolocation,$timeout,$compile)
{

    $scope.windowHeight = window.innerHeight*0.83;
	$scope.phpHost = $rootScope.laravelHost;
	$scope.ActiveTab = $rootScope.DefaultActiveTab;//1;
	$scope.AsianResturantType = 1;
	$scope.cityId = $stateParams.ItemId;

	$scope.selectedCity = $rootScope.selectedCity;
	$scope.enlargedImage = '';
	$scope.cityTracks = [];
	//$scope.LocationTracks = [];
	$scope.currentPlayedTrack = [];
	$scope.dynamicTrack = {};
	$scope.MaxLimit = 6;
	$scope.sitesLimit = 6;
	$scope.resturantLimit = 6;
	$scope.hotelsLimit = 6;
	$scope.shopsLimit = 6;
	$scope.rentalLimit = 6;
	$scope.totalSites = 0;
	$scope.totalResturants  = 0;
	$scope.totalHotels  = 0;
	$scope.totalShops  = 0;
	$scope.totalRentals = 0;
	$scope.CityData = [];
	$scope.MapData = [];
	$scope.cityDescLenth = 0;
	$scope.cityDescMaxLenth = 300;
	$scope.MainTrack = {};
	$scope.currectlyPlayedIndex = -1;

    $scope.MainTrack =
    {
      "url" : ''
    }



  $scope.$on('$ionicView.enter', function(e) {
	  $scope.selectedSound = -1;
  });

	$scope.ActiveTab = $rootScope.DefaultActiveTab;



	$scope.setActiveTab = function(tab)
	{
		//$scope.ActiveTab = 4;
		$scope.ActiveTab = tab;
		$rootScope.DefaultActiveTab = tab;
		$scope.selectedSound = -1;
		MediaManager.stop();
	}

	$scope.setResutrantType = function(tab)
	{
		$scope.AsianResturantType = tab;
	}



  $scope.$watch("ActiveTab", function(newValue, oldValue){

    if ($scope.ActiveTab == 1 || $scope.ActiveTab == 0)
		$scope.getCityDetails(0);

    if ($scope.ActiveTab == 2)
		$scope.getClientCityPlaces();

	if ($scope.ActiveTab == 3)
	{

		//map.remove();
		//document.getElementById("map").remove();
		//$scope.initMapFunc($scope.MapData,32.817317, 35.001799);
	}
	
	if ($scope.ActiveTab == 4)
	{
		$scope.getCityDetails(1);
	}
	

  });


	$scope.getCityDetails = function(setTab)
	{
		$scope.sendparams =
		{
			"id" : $scope.cityId
		}


		//$scope.GetCityDetailsClient = function()
		//{
			//SendPostRequestServer.run($scope.sendparams,$rootScope.laravelHost+'/api/GetCityDetailsClient').then(function(data) {
				$scope.CityData = $rootScope.MainCitiesArray.cities[$scope.cityId].city_data;//data;
				$scope.cityDescLenth = $scope.CityData.info[0].description.length;


				//alert ($scope.CityData.info[0].id);

				//if ($scope.CityData.info[0].id == $scope.cityId)
				$rootScope.CityDetails = $scope.CityData;
			    $scope.cityImage = $rootScope.CityDetails.info[0].image;
				$scope.getClientCityPlaces();
				if (setTab == 0)
				{
					$scope.ActiveTab = 1;
					$rootScope.DefaultActiveTab = 1;					
				}

				$scope.MapLocations = $scope.CityData.map_locations;
				console.log("city data:" ,$scope.CityData );
				$rootScope.CityLocationLat = $scope.CityDetails.info[0].location_lat;
				$rootScope.CityLocationLng = $scope.CityDetails.info[0].location_lng;


				$scope.getMapUserLocation($scope.MapLocations,$scope.CityDetails.info[0].location_lat,$scope.CityDetails.info[0].location_lng);
				//alert ($scope.MapLocations)

				//PlayList
				$rootScope.LocationTracks = [];

				if ($scope.CityData.location_sounds.length > 0)
				{
					for(var i=0;i< $scope.CityData.location_sounds.length;i++)
					{

						$rootScope.LocationTracks.push({
							"url": $rootScope.laravelHost+'/'+$scope.CityData.location_sounds[i].soundfile,
							"artist": "",
							"title": $scope.CityData.location_sounds[i].title,
							"image": $scope.CityData.location_sounds[i].image,
							"sound_length": $scope.CityData.location_sounds[i].sound_length,
							preload: 'metadata'

						});
					}

					if ($rootScope.LocationTracks[0].image)
						$rootScope.cityTrackMainImage = $rootScope.LocationTracks[0].image;
					else
						$rootScope.cityTrackMainImage = $scope.cityImage;

					if ($rootScope.LocationTracks[0].title)
						$rootScope.cityTrackMainTitle = $rootScope.LocationTracks[0].title;


					console.log("$rootScope.LocationTracks : " , $rootScope.LocationTracks )
					//$scope.dynamicTrack = $scope.LocationTracks[0];

				}


				if ($scope.CityData.info[0].soundfile)
				{
					console.log("cityTracks:" , $scope.CityData.info[0])

					$scope.cityTracks = [];
					$scope.cityTracks.push({
						"url": $rootScope.laravelHost+'/'+$scope.CityData.info[0].soundfile,
						"artist": "",
						"title": $scope.CityData.info[0].title,
						"sound_length": $scope.CityData.info[0].sound_length,
						preload: 'metadata'
					});
				}



				$scope.CityGallery = $scope.CityData.gallery;

				$ionicSlideBoxDelegate.update();

				//console.log("Details: " , data);



			//});
		//}

		console.log("CD : " , $scope.CityGallery )
		//alert ($rootScope.CityDetails.length);

	}




	$scope.getClientCityPlaces = function()
	{
		$scope.CityData = $rootScope.CityDetails; //data;
		$scope.cityImage = $rootScope.CityDetails.info[0].image;
		$scope.totalSites = $scope.CityData.sites.length;
		$scope.totalResturants = $scope.CityData.resutrants.length;
		$scope.totalHotels = $scope.CityData.hotels.length;
		$scope.totalShops = $scope.CityData.shops.length;
		$scope.totalRentals = $scope.CityData.rentals.length;
	}

	$scope.changeTrackImage = function(index,item,newimage)
	{
		//MediaManager.stop();
		//$scope.currentPlayedTrack = [];
		//$scope.currentPlayedTrack = item;

		if (newimage)
			$scope.cityTrackMainImage = newimage;
		else
			$scope.cityTrackMainImage = $scope.CityData.info[0].image;

		$scope.MainTrack = item.url;
		$scope.cityTrackMainTitle = item.title;
		$scope.currectlyPlayedIndex = index;
	}

	$scope.showMoreCityDetails = function()
	{
		$scope.cityDescMaxLenth = $scope.cityDescLenth;
	}

	$scope.$watch('currectlyPlayedIndex', function ()
	{
		//MediaManager.stop();
	},false);

	

	$scope.showMore = function(index)
	{
		if (index == 0)
		{
			if ($scope.sitesLimit == $scope.MaxLimit)
				$scope.sitesLimit = $scope.totalSites;
			else
				$scope.sitesLimit = $scope.MaxLimit;
		}

		if (index == 1)
		{
			if ($scope.resturantLimit == $scope.MaxLimit)
				$scope.resturantLimit = $scope.totalResturants;
			else
				$scope.resturantLimit = $scope.MaxLimit;
		}

		if (index == 2)
		{
			if ($scope.hotelsLimit == $scope.MaxLimit)
				$scope.hotelsLimit = $scope.totalHotels;
			else
				$scope.hotelsLimit = $scope.MaxLimit;
		}

		if (index == 3)
		{
			if ($scope.shopsLimit == $scope.MaxLimit)
				$scope.shopsLimit = $scope.totalShops;
			else
				$scope.shopsLimit = $scope.MaxLimit;
		}

		if (index == 4)
		{
			if ($scope.rentalLimit == $scope.MaxLimit)
				$scope.rentalLimit = $scope.totalRentals;
			else
				$scope.rentalLimit = $scope.MaxLimit;
		}
	}






    $scope.stopPlayback = function() {
        MediaManager.stop();
    };
    $scope.playTrack = function(index) {
        $scope.dynamicTrack = $scope.tracks[index];
        $scope.togglePlayback = !$scope.togglePlayback;
    };



    $scope.showCityImageModal = function(newimage)
    {
        $scope.enlargedImage = newimage;

        $ionicModal.fromTemplateUrl('city-image.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(CityImageModal) {
          $scope.CityImageModal = CityImageModal;
          $scope.CityImageModal.show();
         });
    }


    $scope.closeEnlargedImage = function()
    {
        $scope.enlargedImage = '';
        $scope.CityImageModal.hide();
    }


	$scope.playPlayList = function(index)
	{
		//MediaManager.stop();

		$scope.selectedSound = index;

		//MediaManager.destroy();
		$scope.dynamicTrack = {};
		$scope.dynamicTrack = $scope.LocationTracks[index];
		//$scope.togglePlayback = !$scope.togglePlayback;




	}

	/* map tab */


	$scope.geojson = [];


	$scope.getMapUserLocation = function(data,city_lat,city_lng)
	{


			 $timeout(function()
			 {
				$scope.MapData = data;
				if (city_lat)
					$scope.initMapFunc(data,city_lat, city_lng);
				else
					$scope.initMapFunc(data,32.817317, 35.001799);

			 }, 300);


		/*
		if (window.cordova)
		{

			 //$timeout(function()
			// {
				CheckGPS.check(function(){


				var posOptions = {enableHighAccuracy: false};

				$cordovaGeolocation
					.getCurrentPosition(posOptions)
					.then(function (position) {
						$scope.initMapFunc(data,position.coords.latitude, position.coords.longitude);
					}, function (err) {

					});

				  },
				  function(){
							if (data.length > 0)
								$scope.initMapFunc(data,data[0].location_lat, data[0].location_lng);
							else
								$scope.initMapFunc(data,32.817317, 35.001799);
						});
			 //}, 300);
		}
		else
		{

			 $timeout(function()
			 {
				$scope.MapData = data;
				$scope.initMapFunc(data,32.817317, 35.001799);

			 }, 300);
		}
		*/
	}






	$scope.initMapFunc = function(data,lat,lng)
	{

		L.mapbox.accessToken = 'pk.eyJ1Ijoic2hheWxvdXNraSIsImEiOiJjajBoeTdzM3cwMDJuMzNyempvMXVmamdmIn0.t-kt5ED1RkAsTqt1t1MuWA';



		var map = L.mapbox.map('map', 'mapbox.streets')
			.setView([lat, lng], 14);


			$scope.myLayer = L.mapbox.featureLayer().addTo(map);

			//map.legendControl.addLegend("<button id='geolocate' class='findmeButton'>"+$translate.instant('MyLocationText')+"</button>");			


			var geolocate = document.getElementById('geolocate');
			
			if (!navigator.geolocation) {
				//geolocate.innerHTML = '';
			} else {
				geolocate.onclick = function (e) {
					e.preventDefault();
					e.stopPropagation();
					map.locate({timeout: 3000, enableHighAccuracy: false});
				};
			}


			map.on('locationfound', function(e) {
				
				map.fitBounds(e.bounds, 
				{
					maxZoom: 12
				});

				// And hide the geolocation button
				//geolocate.parentNode.removeChild(geolocate);
			});


			map.on('locationerror', function() {
				//geolocate.innerHTML = '';
			});

		$scope.myLayer.on('layeradd', function(e) {
			  var marker = e.layer,
				feature = marker.feature;
			  marker.setIcon(L.icon(feature.properties.icon));
			  if (feature.properties.image)
				  var content = '<div align="center"><div ng-click="navigateDetails('+feature.properties.id+')"><h2 style="text-align: center"> ' + feature.properties.title + '</h2><img src="' + feature.properties.image + '" style="width: 120px;" alt=""></div></div>';
			  else
				  var content = '';
				  var compiledContent = $compile(content)($scope)
					marker.bindPopup(compiledContent[0]);
		});


		$scope.navigateDetails = function(id)
		{
			if (id > 0)
				window.location ="#/app/location_details/"+id;
			//console.log("clicked");
		}

		$scope.myLayer.on('click',function(e) {

		});

		$scope.GetMapLocations(data,lat,lng);
	}


	$scope.GetMapLocations = function(data,lat,lng)
	{
		
		

		$scope.sendparams =
		{
			//"city" : $scope.cityId,
		}
		//SendPostRequestServer.run($scope.sendparams,$rootScope.laravelHost+'/GetCityLocationsClient').then(function(data)
		//{


			var data = data;
			//$rootScope.CityDetailMapArray = data;
			var mapArray = [];


			for(var i=0;i< data.length;i++)
			{

				if (data[i].precise_lat)
				{
					$scope.PreciseMapLat = data[i].precise_lat;
					$scope.PreciseMapLng = data[i].precise_lng;
				}
				else
				{
					$scope.PreciseMapLat = data[i].location_lat;
					$scope.PreciseMapLng = data[i].location_lng;
				}


				var item = {};
				item.type = "Feature";
				item.geometry = { type: 'Point',  coordinates: [$scope.PreciseMapLng,$scope.PreciseMapLat ]}
				item.properties =
				{
				  id : data[i].id,
				  title: data[i].title,
				  image: $rootScope.laravelHost+'/'+data[i].image,
				  description: "",
				  /*
				  'marker-color': '#3ca0d3',
				  'marker-size': 'large',
				  'marker-symbol': 'shop',
				  */
				}

				if (data[i].type == 0)
					$scope.iconUrl = 'img/svg/sightsmapicon.svg';

				else if (data[i].type == 1)
					$scope.iconUrl = 'img/svg/foodmapicon.svg';

				else if (data[i].type == 2)
					$scope.iconUrl = 'img/svg/hotelsmapicon.svg';

				else if (data[i].type == 3)
					$scope.iconUrl = 'img/svg/sightsmapicon.svg';

				else if (data[i].type == 4)
					$scope.iconUrl = 'img/svg/sightsmapicon.svg';

				else
					$scope.iconUrl = 'img/svg/sightsmapicon.svg';

				item.properties.icon =
				{

					iconUrl: $scope.iconUrl,
					//iconUrl: $rootScope.laravelHost+'/'+data[i].image,
					iconSize: [50, 50], // size of the icon
					iconAnchor: [25, 25], // point of the icon which will correspond to marker's location
					popupAnchor: [0, -25], // point from which the popup should open relative to the iconAnchor
					className: 'dot'

				}
				mapArray.push(item);

			}

				

				$scope.errorLocation = function()
				{
					$scope.geojson = mapArray;//$rootScope.CityDetailMapArray; //mapArray;

					$scope.myLayer.setGeoJSON($scope.geojson);
					$rootScope.CityDetailMapArray = $scope.geojson;
				}

				$scope.successLocation = function(lat,lng)
				{
					

					
					var item = {};
					item.type = "Feature";
					item.geometry = { type: 'Point',  coordinates: [lat,lng ]}

					item.properties =
					{
					  id : 0,
					  title: $translate.instant('MyLocationText'),
					  description: "",
					  /*
					  'marker-color': '#3ca0d3',
					  'marker-size': 'large',
					  'marker-symbol': 'shop',
					  */
					}

					item.properties.icon =
					{

						iconUrl: 'img/blue-dot.png',
						iconSize: [50, 50], // size of the icon
						iconAnchor: [25, 25], // point of the icon which will correspond to marker's location
						popupAnchor: [0, -25], // point from which the popup should open relative to the iconAnchor
						className: 'dot'

					}
					
					mapArray.push(item);

					
					$scope.geojson = mapArray;//$rootScope.CityDetailMapArray; //mapArray;
					console.log("J3 : " ,$scope.geojson);

					
					 $timeout(function()
					 {
						 $scope.myLayer.setGeoJSON($scope.geojson);
					 }, 300);


					$rootScope.CityDetailMapArray = $scope.geojson;

				}




					if (window.cordova)
					{
						CheckGPS.check(function(){


						var posOptions = {enableHighAccuracy: false};

						$cordovaGeolocation
							.getCurrentPosition(posOptions)
							.then(function (position) {
								$scope.successLocation(position.coords.longitude,position.coords.latitude );
							}, function (err) {
									$scope.errorLocation();
							});

						  },
						  function(){
									$scope.errorLocation();
								});
					}
					else
					{
						 $scope.errorLocation();
						//$scope.successLocation(32.817317, 35.001799 );
					}

	//});
	}	


	$scope.$on("$ionicView.afterLeave", function(event, data){

		MediaManager.stop();
	});



	document.addEventListener("pause", $scope.stopPlayback, false);
})


.controller('LocationDetailsCtrl', function($scope,$rootScope,$stateParams,$localStorage,$ionicSideMenuDelegate,SendGetRequestServer,SendPostRequestServer,$ionicPopup,$ionicModal,$translate,MediaManager,$ionicSlideBoxDelegate,$compile,$cordovaGeolocation,$timeout,$ionicHistory,$state,$ionicPlatform)
{
	$scope.phpHost = $rootScope.laravelHost;
	$scope.LocationId = $stateParams.ItemId;
	$scope.ratingsArray = [];
	$scope.LocationTracks = [];
	$scope.MapLocations = [];
	$scope.enlargedImage = '';
	$scope.LocationDescLenth = 0;
	$scope.maxLocationDescLenth = 300;
	$scope.LocationShareText = '';
	console.log("LocationDetailsCtrl")

	$scope.getClientLocationDetails = function()
	{
		$scope.sendparams =
		{
			"id" : $scope.LocationId
		}
		SendPostRequestServer.run($scope.sendparams,$rootScope.laravelHost+'/api/getClientLocationDetails').then(function(data) {
			$scope.LocationDataArray = data;

			$scope.LocationDescLenth = $scope.LocationDataArray.info[0].description.length;

			$scope.LocationShareText = '我在“自游以色列”APP上发现了游览以色列 ' +$scope.LocationDataArray.info[0].title+' 的绝佳实用信息。马上点击 www.goyiselie.com 免费下载“自游以色列”APP，跟我一起探索吧！';
			
			$scope.LocationGallery = $scope.LocationDataArray.gallery;
			$ionicSlideBoxDelegate.update();
			console.log("location dataShare:" , $scope.LocationDataArray);

			if ($scope.LocationDataArray.location_reviews.length > 0)
				$scope.ratingsArray = $scope.LocationDataArray.location_reviews;

			console.log("LocationDataArray: " , data);
			//alert ($scope.LocationDataArray.info[0].location_lat);
			//alert ($scope.LocationDataArray.info[0].location_lng);
			$scope.MapLocations = [];
			$scope.MapLocations = data.map_locations;//$scope.LocationDataArray.map_locations;


			if ($rootScope.CityLocationLat)
				$scope.initMapFun($scope.MapLocations,$rootScope.CityLocationLat,$rootScope.CityLocationLng);
			else
				$scope.initMapFun($scope.MapLocations,32.800772,35.008365);

			/*
			if (window.cordova)
			{


					CheckGPS.check(function(){


					var posOptions = {enableHighAccuracy: false};

					$cordovaGeolocation
						.getCurrentPosition(posOptions)
						.then(function (position) {
							$scope.initMapFun($scope.MapLocations,position.coords.latitude, position.coords.longitude);
						}, function (err) {
							if ($scope.LocationDataArray.info[0].location_lat)
								$scope.initMapFun($scope.MapLocations,$scope.LocationDataArray.info[0].location_lat,$scope.LocationDataArray.info[0].location_lng);
							else
								$scope.initMapFun($scope.MapLocations,$scope.MapLocations[0].location_lat,$scope.MapLocations[0].location_lng);
						});

					  },
					  function(){
							if ($scope.LocationDataArray.info[0].location_lat)
								$scope.initMapFun($scope.MapLocations,$scope.LocationDataArray.info[0].location_lat,$scope.LocationDataArray.info[0].location_lng);
							else
								$scope.initMapFun($scope.MapLocations,$scope.MapLocations[0].location_lat,$scope.MapLocations[0].location_lng);
							});
				 //}, 300);
			}
			else
			{

				 $timeout(function()
				 {
					 $scope.initMapFun($scope.MapLocations,$scope.MapLocations[0].location_lat,$scope.MapLocations[0].location_lng);
				 }, 300);
			}
			*/
			$scope.LocationTracks = [];
			if ($scope.LocationDataArray.location_sounds.length > 0)
			{
				for(var i=0;i< $scope.LocationDataArray.location_sounds.length;i++)
				{

					$scope.LocationTracks.push({
						"url": $rootScope.laravelHost+'/'+$scope.LocationDataArray.location_sounds[i].soundfile,
						"artist": "",
						"title": $scope.LocationDataArray.location_sounds[i].title,
						"sound_length": $scope.LocationDataArray.location_sounds[i].sound_length,
					});
				}

				//console.log("LocationTracks" , LocationTracks)
			}
			/*
			if ($scope.LocationDataArray.info[0].soundfile)
			{
				$scope.LocationTracks = [];
				$scope.LocationTracks.push({
					"url": $rootScope.laravelHost+'/'+$scope.LocationDataArray.info[0].soundfile,
					"artist": "",
					"title": "",
				});
			}
			*/


		});
	}

	$scope.showMoreBtn = function()
	{
		$scope.maxLocationDescLenth = $scope.LocationDescLenth;
	}

	$scope.getClientLocationDetails();

	$scope.addReview  = function()
	{
	  $scope.data = {};

	  // An elaborate, custom popup
		$ionicPopup.show({
		template: '<textarea placeholder="'+$translate.instant('inputreviewtext')+'" style="direction:ltr;" rows="4" cols="50" ng-model="data.text" ></textarea>',

		title: $translate.instant('inputreviewtext'),
		//subTitle: 'Please use normal things',
		scope: $scope,
		buttons: [
		  { text: $translate.instant('canceltext') },
		  {
			text: $translate.instant('sendtext'),
			type: 'button-positive',
			onTap: function(e) {
			  if (!$scope.data.text)
			  {
				//e.preventDefault();
			  } else
			  {

				//insert comment.

			$scope.date = new Date()
			$scope.hours = $scope.date.getHours()
			$scope.minutes = $scope.date.getMinutes()

			if ($scope.hours < 10)
			$scope.hours = " " + $scope.hours

			if ($scope.minutes < 10)
			$scope.minutes = "0" + $scope.minutes
			$scope.time = $scope.hours+':'+$scope.minutes;


			$scope.sendparams =
			{
				"id" : $scope.LocationId,
				"comment" : $scope.data.text,
				"time" : $scope.time
			}



			SendPostRequestServer.run($scope.sendparams,$rootScope.laravelHost+'/api/AddLocationReview').then(function(data)
			{
					$scope.ratingsArray.push({
						"comment": $scope.data.text,
						"time": $scope.time
					});
			 });
			  }
			}
		  }
		]
	  });
	}


	$scope.dialPhone = function(phone)
	{
		window.open('tel:' + phone, '_system');
	}


	$scope.openWebSite = function(website)
	{
		iabRef = window.open(website, '_blank', 'location=yes');
	}


	$scope.openMail = function(mail)
	{
		window.location = "mailto:"+mail;
	}

	$scope.shareLocationDetails = function()
	{
		window.plugins.socialsharing.share($scope.LocationShareText)
		//window.plugins.socialsharing.share($scope.LocationDataArray.info[0].title+ ' ' +$scope.LocationDataArray.info[0].phone)
	}


    $scope.showLocationImageModal = function(newimage)
    {
        $scope.enlargedImage = newimage;

        $ionicModal.fromTemplateUrl('location-image.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(LocationImageModal) {
          $scope.LocationImageModal = LocationImageModal;
          $scope.LocationImageModal.show();
         });
    }


    $scope.closeEnlargedImage = function()
    {
        $scope.enlargedImage = '';
        $scope.LocationImageModal.hide();
    }

	/* map tab */


	$scope.geojson3 = [];



	$scope.GetCityLocationsClient = function(data,lat,lng)
	{


		$scope.sendparams =
		{
			//"city" : 1,//$scope.cityId,
		}

		//SendPostRequestServer.run($scope.sendparams,$rootScope.laravelHost+'/GetCityLocationsClient').then(function(data) {
			console.log("E1");
			var mapArray = [];

			console.log("locations: " , data);

			for(var i=0;i< data.length;i++)
			{
				if (data[i].precise_lat)
				{
					$scope.PreciseMapLat = data[i].precise_lat;
					$scope.PreciseMapLng = data[i].precise_lng;
				}
				else
				{
					$scope.PreciseMapLat = data[i].location_lat;
					$scope.PreciseMapLng = data[i].location_lng;
				}

				var item = {};
				item.type = "Feature";
				item.geometry = { type: 'Point',  coordinates: [$scope.PreciseMapLng,$scope.PreciseMapLat ]}
				item.properties =
				{
				  id : data[i].id,
				  title: data[i].title,
				  image: $rootScope.laravelHost+'/'+data[i].image,
				  description: "",
				  /*
				  'marker-color': '#3ca0d3',
				  'marker-size': 'large',
				  'marker-symbol': 'shop',
				  */
				}

				if (data[i].type == 0)
					$scope.iconUrl = 'img/svg/sightsmapicon.svg';

				else if (data[i].type == 1)
					$scope.iconUrl = 'img/svg/foodmapicon.svg';

				else if (data[i].type == 2)
					$scope.iconUrl = 'img/svg/hotelsmapicon.svg';

				else if (data[i].type == 3)
					$scope.iconUrl = 'img/svg/sightsmapicon.svg';

				else if (data[i].type == 4)
					$scope.iconUrl = 'img/svg/sightsmapicon.svg';

				else
					$scope.iconUrl = 'img/svg/sightsmapicon.svg';

				item.properties.icon =
				{

					iconUrl: $scope.iconUrl,
					//iconUrl: $rootScope.laravelHost+'/'+data[i].image,
					iconSize: [50, 50], // size of the icon
					iconAnchor: [25, 25], // point of the icon which will correspond to marker's location
					popupAnchor: [0, -25], // point from which the popup should open relative to the iconAnchor
					className: 'dot'

				}
				mapArray.push(item);

			}



				$scope.errorLocation = function()
				{
					$scope.geojson3 = mapArray;//$rootScope.CityDetailMapArray; //mapArray;
					console.log("J3 : " ,$scope.geojson3);
					$scope.myLayer3.setGeoJSON($scope.geojson3);

				}

				$scope.successLocation = function(lat,lng)
				{
					//alert (pos.coords.latitude);
					//alert (pos.coords.longitude);

					var item = {};
					item.type = "Feature";
					item.geometry = { type: 'Point',  coordinates: [lat,lng ]}

					item.properties =
					{
					  id : 0,
					  title: $translate.instant('MyLocationText'),
					  description: "",
					  /*
					  'marker-color': '#3ca0d3',
					  'marker-size': 'large',
					  'marker-symbol': 'shop',
					  */
					}

					item.properties.icon =
					{

						iconUrl: 'img/blue-dot.png',
						iconSize: [50, 50], // size of the icon
						iconAnchor: [25, 25], // point of the icon which will correspond to marker's location
						popupAnchor: [0, -25], // point from which the popup should open relative to the iconAnchor
						className: 'dot'

					}

					mapArray.push(item);


					$scope.geojson3 = mapArray;//$rootScope.CityDetailMapArray; //mapArray;
					console.log("J3 : " ,$scope.geojson3);
					$scope.myLayer3.setGeoJSON($scope.geojson3);

				}

					if (window.cordova)
					{
						CheckGPS.check(function(){


						var posOptions = {enableHighAccuracy: false};

						$cordovaGeolocation
							.getCurrentPosition(posOptions)
							.then(function (position) {
								$scope.successLocation( position.coords.longitude,position.coords.latitude);
							}, function (err) {
									$scope.errorLocation();
							});

						  },
						  function(){
									$scope.errorLocation();
								});
					}
					else
					{
						$scope.errorLocation();
					}


		//});
	}

		$scope.initMapFun = function(data,lat,lng)
		{



			L.mapbox.accessToken = 'pk.eyJ1Ijoic2hheWxvdXNraSIsImEiOiJjajBoeTdzM3cwMDJuMzNyempvMXVmamdmIn0.t-kt5ED1RkAsTqt1t1MuWA';

			var map3 = L.mapbox.map('map3', 'mapbox.streets')
				.setView([lat,lng], 13);


			$scope.myLayer3 = L.mapbox.featureLayer().addTo(map3);
			
			
			//map3.legendControl.addLegend("<button id='geolocate' class='findmeButton'><img src='img/svg/map_center.svg'/></button>");			


			var geolocate = document.getElementById('geolocate');
			
			if (!navigator.geolocation) {
				//geolocate.innerHTML = '';
			} else {
				geolocate.onclick = function (e) {
					e.preventDefault();
					e.stopPropagation();
					//
					map3.locate({timeout: 3000, enableHighAccuracy: false});
				};
			}


			map3.on('locationfound', function(e) {

				
				map3.fitBounds(e.bounds, 
				{
					maxZoom: 12
				});


				// And hide the geolocation button
				//geolocate.parentNode.removeChild(geolocate);
			});


			map3.on('locationerror', function() {
				//geolocate.innerHTML = '';
			});



			$scope.myLayer3.on('layeradd', function(e) {
				  var marker = e.layer;
					feature = marker.feature;
					marker.setIcon(L.icon(feature.properties.icon));
					if (feature.properties.image)
						var content = '<div align="center"><div ng-click="navigateDetails('+feature.properties.id+')"> <h2 style="text-align: center"> ' + feature.properties.title + '</h2><img src="' + feature.properties.image + '" alt="" style="width:120px;"></div></div>';
					else
						var content = '';
					
					var compiledContent = $compile(content)($scope)
					marker.bindPopup(compiledContent[0]);
			});


			$scope.myLayer3.on('click',function(e) {


				//e.layer.closePopup();
				/*
				var feature = e.layer.feature;
				//alert (feature.properties.id);
				//window.location ="#/app/location_details/"+feature.properties.id;
				var content = '<div><strong>' + feature.properties.title + '</strong>' +
							  '<p>' + feature.properties.description + '</p></div>';

				info.innerHTML = content;
				*/
			});


			$scope.GetCityLocationsClient(data,lat,lng);

		}




	//$scope.GetCityLocationsClient();

	$scope.stopPlayback = function()
	{
		MediaManager.stop();
	}


	$scope.navigateDetails = function (id)
	{
		if (id != $scope.LocationId && id > 0)
		{
			map3.remove();
			window.location ="#/app/location_details/"+id;
		}
	}


	$scope.$on("$ionicView.afterLeave", function(event, data){

		MediaManager.stop();
	});

	$scope.LocationBack = function()
	{
		 map3.remove();
		 $ionicHistory.goBack();
		//window.history.back();
	}


	document.addEventListener("pause", $scope.stopPlayback, false);
	
	
	$ionicPlatform.registerBackButtonAction(
	function () 
	{
	 if( $state.current.name == "app.location_details")
	 {
		$scope.LocationBack();
	 }
	 else
	 {
		 $scope.goBackButton();
	 }
	}, 100
    );

	

})


//fix later
.controller('CityDetailsMap', function($scope,$rootScope,$stateParams,$localStorage,$ionicSideMenuDelegate,SendGetRequestServer,SendPostRequestServer,$ionicPopup,$ionicModal,$translate,MediaManager,$ionicSlideBoxDelegate,$compile,$timeout)
{
	$scope.phpHost = $rootScope.laravelHost;
	$scope.cityId = $stateParams.ItemId;
	$scope.geojson = [];

	//console.log("Data : ");
	$rootScope.$watch('CityDetailMapArray', function ()
	{
		$scope.geojson = $rootScope.CityDetailMapArray;
		if($scope.geojson.length > 0)
			$scope.setDataOnMap();
	},false);



	$scope.setDataOnMap = function()
	{

			L.mapbox.accessToken = 'pk.eyJ1Ijoic2hheWxvdXNraSIsImEiOiJjajBoeTdzM3cwMDJuMzNyempvMXVmamdmIn0.t-kt5ED1RkAsTqt1t1MuWA';
			//32.817317, 35.001799
			var map2 = L.mapbox.map('map2', 'mapbox.streets')
			.setView([$rootScope.CityLocationLat, $rootScope.CityLocationLng], 14);


			
			//map2.legendControl.addLegend("<button id='geolocate' class='findmeButton'>"+$translate.instant('MyLocationText')+"</button>");			
	
			var geolocate = document.getElementById('geolocate');

			if (!navigator.geolocation) {
				//geolocate.innerHTML = '';
			} else {
				geolocate.onclick = function (e) {
					e.preventDefault();
					e.stopPropagation();
					map2.locate({timeout: 3000, enableHighAccuracy: false});
				};
			}


			map2.on('locationfound', function(e) {

				map2.fitBounds(e.bounds, 
				{
					maxZoom: 12
				});				


				// And hide the geolocation button
				//geolocate.parentNode.removeChild(geolocate);
			});


			map2.on('locationerror', function() {
				//geolocate.innerHTML = '';
			});

			

			$scope.myLayer2 = L.mapbox.featureLayer().addTo(map2);

				$scope.myLayer2.on('layeradd', function(e) {
				  var marker = e.layer,
					feature = marker.feature;
				  marker.setIcon(L.icon(feature.properties.icon));
				  if (feature.properties.image)
					var content = '<div align="center"><div ng-click="navigateDetails('+feature.properties.id+')"><h2 style="text-align: center">' + feature.properties.title + '</h2><img src="' + feature.properties.image + '" style="width: 120px;" alt=""></div></div>';
				else
					var content = '';
					var compiledContent = $compile(content)($scope)
					marker.bindPopup(compiledContent[0]);
			});


			$scope.myLayer2.on('click',function(e) {
				/*
				e.layer.closePopup();

				var feature = e.layer.feature;
				//alert (feature.properties.id);
				window.location ="#/app/location_details/"+feature.properties.id;
				var content = '<div><strong>' + feature.properties.title + '</strong>' +
							  '<p>' + feature.properties.description + '</p></div>';

				//info.innerHTML = content;
				*/
			});
			
			 
			
			 $timeout(function()
			 {
				$scope.geojson = $rootScope.CityDetailMapArray;
				if ($scope.myLayer2)
					$scope.myLayer2.setGeoJSON($scope.geojson);
			 }, 300);
			 

		}

		$scope.navigateDetails = function (id)
		{
			window.location ="#/app/location_details/"+id;
			//console.log('clicked');
		}
})

.controller('MoneyExchangeCtrl', function($scope,$rootScope,$stateParams,$localStorage,$ionicSideMenuDelegate,SendGetRequestServer,SendPostRequestServer,$ionicPopup,$ionicModal,$translate,MediaManager,$ionicSlideBoxDelegate)
{
	$scope.fields =
	{
		"fromcurrency" : "0",
		"tocurrency" : "3",
		"amount" : "",
		"result" : ""
	}


   //calculate currency
  $scope.$watch("fields", function(newValue, oldValue){

	//console.log($scope.fields)

	if ($scope.fields.tocurrency == 0)
		$scope.fields.tocurrencyValue = 1;

	 if ($scope.fields.tocurrency == 1)
		$scope.fields.tocurrencyValue = 3.67;

	 if ($scope.fields.tocurrency == 2)
		$scope.fields.tocurrencyValue = 3.93;

	 if ($scope.fields.tocurrency == 3)
		$scope.fields.tocurrencyValue = 0.525;


	if ($scope.fields.fromcurrency == 0)
		$scope.fields.fromcurrencyValue = 1;

	 if ($scope.fields.fromcurrency == 1)
		$scope.fields.fromcurrencyValue = 3.67;

	 if ($scope.fields.fromcurrency == 2)
		$scope.fields.fromcurrencyValue = 3.93;

	if ($scope.fields.fromcurrency == 3)
		$scope.fields.fromcurrencyValue = 0.525;

	 $scope.fields.result = $scope.fields.amount/$scope.fields.tocurrencyValue*$scope.fields.fromcurrencyValue;

	 if($scope.fields.result == 0 )
     $scope.fields.result = '';

	}, true);

})

.controller('TranslationsCtrl', function($scope,$rootScope,$stateParams,$localStorage,$ionicSideMenuDelegate,SendGetRequestServer,SendPostRequestServer,$ionicPopup,$ionicModal,$translate,MediaManager,$ionicSlideBoxDelegate,$timeout)
{
	$scope.phpHost = $rootScope.laravelHost;
	$scope.ActiveTab = 0;
	$scope.soundsArray = [];
	$scope.TrackList = [];
	$scope.translationsCategories = $rootScope.MainCitiesArray.voicetranslator;


	/* for testing
	$scope.sendparams = {};
	SendGetRequestServer.run($scope.sendparams,$rootScope.laravelHost+'/GetCitiesClient').then(function(data) {
		$scope.translationsCategories = data.voicetranslator;
		$scope.soundsArray = $scope.translationsCategories[0];
		$scope.getTracks();
		console.log(data);
	});
	*/

	$scope.getTracks = function()
	{
		$scope.TrackList = [];
		if ($scope.soundsArray.sounds.length > 0)
		{
			for(var i=0;i< $scope.soundsArray.sounds.length;i++)
			{
				$scope.TrackList.push({
					"url": $rootScope.laravelHost+'/'+$scope.soundsArray.sounds[i].soundfile,
					"artist": "",
					"title": $scope.soundsArray.sounds[i].title
				});
			}
		}
	}


	$scope.selectCat = function(index)
	{
		$scope.soundsArray = $scope.translationsCategories[index];
		$scope.getTracks();
	}

	$scope.selectCat(0);


	$scope.$on("$ionicView.afterLeave", function(event, data){

		MediaManager.stop();
	});

	$scope.stopPlayback = function()
	{
		MediaManager.stop();
	}

	document.addEventListener("pause", $scope.stopPlayback, false);


})

.filter('stripslashes', function () {
    return function (value)
	{
		if(value)
		{
			return (value + '')
			.replace(/\\(.?)/g, function(s, n1) {
			  switch (n1) {
				case '\\':
				  return '\\';
				case '0':
				  return '\u0000';
				case '':
				  return '';
				default:
				  return n1;
			  }
			});
		}
		else
		{
			return ("");
		}
    };
})



.filter('range', function () {
    return function (input, total)
	{
    total = parseInt(total);

    for (var i=0; i<total; i++) {
      input.push(i);
    }


        return input;
    };
})


.filter('toTrusted', function ($sce)
{
    return function (value) {
        return $sce.trustAsHtml(value);
    };
})


.directive('imageonload', function($ionicLoading,$rootScope) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {
                //alert('image is loaded');
				$rootScope.$broadcast('imageloaded')
            });
        }
    };
})
