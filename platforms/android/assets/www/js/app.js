// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js


//'google.places'
angular.module('starter', ['ionic', 'starter.controllers','starter.factories','ngStorage','ngCordova', 'pascalprecht.translate', 'ionic-audio','ionic.ion.imageCacheFactory'])

.run(function($ionicPlatform,$rootScope,$http,$localStorage, $window,$cordovaGeolocation,$ionicPopup,$translate,SendGetRequestServer) {

	$rootScope.DefaultLanguage = 'ch';
	$rootScope.DefaultActiveTab = 1;

	$rootScope.selectedCity = '';
	$rootScope.CityDetailMapArray = [];
	//$rootScope.laravelHost = "http://localhost:8000/";
  $rootScope.laravelHost = "http://ziyou.tapper.co.il/";
  //$rootScope.laravelHost = "http://ziyou.bnyah.co.il/";
  $rootScope.CityDetails = [];
	$rootScope.MainCitiesArray = [];
	$rootScope.LocationTracks = [];
	$rootScope.CityLocationLat = '';
	$rootScope.CityLocationLng = '';

  $ionicPlatform.ready(function() {

	//get weather
	var sendparams = {};
	var lat = '32.083589';
	var lng = '34.798270';
	SendGetRequestServer.run(sendparams,'http://api.openweathermap.org/data/2.5/weather?lat='+lat+'&lon='+lng+'&appid=d954ae2732549b677fc73fcd1031b440&units=metric').then(function(data) {
		$rootScope.CurrentWeather = data.main.temp;

	});

	$rootScope.checkGPS = function()
	{
		if (window.cordova)
		{

			CheckGPS.check(function()
			  {
			  },
			  function(){

				var myPopup = $ionicPopup.show({
				title: $translate.instant('Turnongpstext'),
				scope: $rootScope,
				cssClass: 'custom-popup',
				buttons: [

			   {
				text: $translate.instant('Settingstext'),
				type: 'button-positive',
				onTap: getLocation
			   },
			   {
				text: $translate.instant('Canceltext'),
				type: 'button-positive',
				onTap: function(e) {
				  //alert (1)
				}
			   },
			   ]
			  });

			  });
		}
		}

	$rootScope.checkGPS();



	function getLocation()
	{
		 if (ionic.Platform.isIOS() == true)
			 cordova.plugins.diagnostic.switchToSettings();
		 else
			 cordova.plugins.diagnostic.switchToLocationSettings();
	}

	document.addEventListener("resume", $rootScope.checkGPS, false);

/*
	if(typeof navigator.globalization !== "undefined") {
		navigator.globalization.getPreferredLanguage(function(language) {
			$translate.use((language.value).split("-")[0]).then(function(data) {
				console.log("SUCCESS -> " + data);
			}, function(error) {
				console.log("ERROR -> " + error);
			});
		}, null);
	}
*/


   window.plugins.OneSignal
  .startInit("f6c1fd0c-676c-4eac-b2d4-7f4d554a3a47")
  .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.None)	
  .endInit();
  
  


    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }



  });
})





.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider,$translateProvider) {

  // configures staticFilesLoader
  $translateProvider.useStaticFilesLoader({
    prefix: 'js/data/locale-',
    suffix: '.json'
  });
  // load 'en' table on startup
  $translateProvider.preferredLanguage('ch');
  $translateProvider.fallbackLanguage('ch');


	$ionicConfigProvider.backButton.previousTitleText(false).text('');
  $stateProvider

  .state('app',
  {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.main', {
    url: '/main',
    views: {
      'menuContent': {
        templateUrl: 'templates/main.html',
		controller: 'MainCtrl'
      }
    }
  })

  .state('app.citydetails', {
    url: '/citydetails/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/city_details.html',
		controller: 'CityDetailsCtrl'
      }
    }
  })



  .state('app.location_details', {
    url: '/location_details/:ItemId',
	//cache:false,
    views: {
      'menuContent': {
        templateUrl: 'templates/location_details.html',
		controller: 'LocationDetailsCtrl'
      }
    }
  })

  .state('app.moneyexchange', {
    url: '/moneyexchange',
    views: {
      'menuContent': {
        templateUrl: 'templates/money_exchange.html',
		controller: 'MoneyExchangeCtrl'
      }
    }
  })

  .state('app.traslations', {
    url: '/traslations',
    views: {
      'menuContent': {
        templateUrl: 'templates/traslations.html',
		controller: 'TranslationsCtrl'
      }
    }
  })
;







  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/main');
})


